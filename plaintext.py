#!/usr/bin/env python3

from flask import Flask, request
import uuid
import hashlib
from jinja2 import Environment, FileSystemLoader
jenv = Environment(loader=FileSystemLoader("."))

app = Flask(__name__)

auth_tokens = {}

PASSWD = eval(open("passwd").read())

@app.route("/")
def slash():
    return jenv.get_template("plaintext.html").render()

@app.route("/passwd")
def passwd():
    return jenv.get_template("passwd").render()

@app.route("/auth/<user>", methods=["POST"])
def auth(user):
    "Return the auth cookie value"
    global auth_tokens
    print("GOT A POST")
    user = request.json["user"]
    pwd = request.json["passwd"]
    assert user in PASSWD, f"Unknown user {user}"
    assert PASSWD[user] == pwd, f"{user}'s password {pwd} does not match the one in /passwd"
    answer = str(uuid.uuid4())
    auth_tokens[user] = answer
    return {"token": answer}

@app.route("/secret")
def secret():
    "Return the secret, if the cookie is set"
    user = request.cookies.get("user")
    token = request.cookies.get("token")
    assert user in PASSWD, f"Unknown user {user}"
    assert user in auth_tokens, f"Unlogged user {user}, passwd is {auth_tokens}"
    assert auth_tokens[user] == token, f"Invalid token {token} for {user}, passwd is {auth_tokens}"
    return hashlib.sha256(user.encode()+open("salt","rb").read()).hexdigest()

@app.route("/failed")
def failed():
    return "Login failed, password does not match the one in /passwd"
