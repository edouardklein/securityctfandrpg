#!/usr/bin/env python3
from flask import Flask, request, send_file
from jinja2 import Environment, FileSystemLoader
from secret import poc
import json
from matplotlib import pyplot as plt
import seaborn as sns
import pandas as pd
sns.set()
from collections import defaultdict
import datetime

jenv = Environment(loader=FileSystemLoader("."))

app = Flask(__name__)

pwnage = json.load(open("pwnage.json"))

def update_pwnage(who, target, host, proof):
    """Tell us who powned whom"""
    global pwnage
    key = f"{who} powned {target} on {host}"
    if key in pwnage:
        return
    pwnage[key] = [datetime.datetime.now().timestamp(), proof]
    with open("pwnage.json", "w") as f:
        json.dump(pwnage, f)
    draw_scores()


@app.route("/")
def slash():
    return jenv.get_template("score.html").render()

@app.route("/updatescore", methods=["POST"])
def update():
    "Update the score"
    global pwnage
    who = request.json["who"]
    target = request.json["target"]
    host = request.json["host"]
    proof = request.json["proof"]
    salt = open(f"{host}/salt").read().strip()
    assert proof == poc(target, host, salt), "Incorrect proof"
    update_pwnage(who, target, host, proof)
    return {"OK":"OK"}

@app.route('/scores.png')
def get_image():
    return send_file("scores.png", mimetype='image/png')

def chronology(d):
    answer = []
    for s, [t, _] in d.items():
        who = s.split()[0]
        target = s.split()[4]
        answer += [[who, target, t]]
    return sorted(answer, key=lambda x: x[-1])

def draw_scores():
    df = pd.DataFrame(columns=['date', 'team', 'score'])
    score = defaultdict(int)
    for winner, loser, time in chronology(pwnage):
        score[winner] += 1
        score[loser] -= 3
        df = df.append({'date': time, 'team': winner, 'score': score[winner]}, ignore_index=True)
        df = df.append({'date': time, 'team': loser, 'score': score[loser]}, ignore_index=True)
    plt.figure()
    sns.lineplot(data=df, hue='team', x='date', y='score')
    plt.savefig("scores.png")
