#!/usr/bin/env python3

from flask import Flask, request
import uuid
import hashlib
from jinja2 import Environment, FileSystemLoader
from secret import secret_for

jenv = Environment(loader=FileSystemLoader("."))

app = Flask(__name__)

auth_tokens = {}

PASSWD = eval(open("passwd").read())

@app.route("/")
def slash():
    return jenv.get_template("client.html").render()

@app.route("/auth/<user>", methods=["POST"])
def auth(user):
    "Return the auth cookie value"
    global auth_tokens
    user = request.json["user"]
    pwd = request.json["passwd"]
    assert user in PASSWD, f"Unknown user {user}"
    assert PASSWD[user] == pwd, f"{user}'s password {pwd} is not correct"
    answer = str(uuid.uuid4())
    auth_tokens[user] = answer
    return {"token": answer}

@app.route("/secret")
def secret():
    "Return the secret, if the cookie is set"
    user = request.cookies.get("user")
    token = request.cookies.get("token")
    assert user in PASSWD, f"Unknown user {user}"
    assert user in auth_tokens, f"Unlogged user {user}, passwd is {auth_tokens}"
    assert auth_tokens[user] == token, f"Invalid token {token} for {user}, passwd is {auth_tokens}"
    secret = f"{user} on {open('/etc/hostname').read().strip()} " + secret_for(user)
    print(f"SECRET REVEALED: {secret}")
    return secret

@app.route("/failed")
def failed():
    return "Login failed"
