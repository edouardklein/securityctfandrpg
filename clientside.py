#!/usr/bin/env python3

from flask import Flask, request
import uuid
import hashlib
from jinja2 import Environment, FileSystemLoader
jenv = Environment(loader=FileSystemLoader("."))

app = Flask(__name__)

auth_tokens = {}

PASSWD = eval(open("passwd").read())

@app.route("/")
def slash():
    return jenv.get_template("clientside.html").render(
        passwd=PASSWD)

@app.route("/auth/<user>")
def auth(user):
    "Return the auth cookie value"
    global auth_tokens
    answer = str(uuid.uuid4())
    assert user in PASSWD, f"Unknown user {user}"
    auth_tokens[user] = answer
    return {"token": answer}

@app.route("/secret")
def secret():
    "Return the secret, if the cookie is set"
    user = request.cookies.get("user")
    token = request.cookies.get("token")
    assert user in PASSWD, f"Unknown user {user}"
    assert user in auth_tokens, f"Unlogged user {user}, passwd is {auth_tokens}"
    assert auth_tokens[user] == token, f"Invalid token {token} for {user}, passwd is {auth_tokens}"
    return hashlib.sha256(user.encode()+open("salt","rb").read()).hexdigest()

@app.route("/failed")
def failed():
    return "Login failed"
