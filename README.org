#+title: IT Security 201

* Introduction to participants
- Mix of CTF and RPG
  - CTF
    - Explain what a CTF is, give examples
    - Rules:
      - Must run and make your VM available
        - - (# other teams x # app user)  points per 10 min of unavailability (unless authorized in the RPG)
      - Attack only the application layer
        - Because the bottom layers are too difficult to set up in a simulation
        - Also it is the most likely to be exploitable in real life
      - 1 point/ other team's user secret revealed on =suc=
      - - 3 points / own team's user secret revealed on =suc=
  - Role Playing Game
    - Ask via =suc= for
      - defense product/services/training/policy you can
        - buy on the market
        - get internally
      - attack services
        - you can get on the black market
        - fall outside the rules of engagement of the CTF
      - both will have consequences on the CTF
      - (# other teams x # app user) points for the least money spent
      - - (# other teams x # app user) points for the most money spent
      - Effect may be random
      - The more specific an attack is, the more likely you are to fall between the cracks of the other team's defences
  - Give example:
    - [RPG] Alice pays a hacker to leak the passwords of bob's app
    - [RPG+CTF] Alice gets the leaked passwords
    - [CTF] Alice begins to get the secrets
    - [RPG] the hackers tries to ransom bob for more money
    - [RPG] aware of the leak, bob requires a password reset
    - [CTF] the password reset is done, alice's db is now useless.
  - Your users are the dumbest motherfuckers. They'll do everything wrong unless you train them
    - passwork sharing
    - losing unencrypted laptops
    - Installing personal stuff on work computers
    - No screenlocks
  - You might be powned without knowing it
- Debrief
  - Winner for the day
  - Overall ranking
  - Techniques used
    - Attack
    - Effective countermeasures
  - Lessons learned
* Proposed rounds
- 1 :: Client side checking
- 2 :: Plain text DB
- 3 :: Hash but no salt
- 4 :: Salted hash, no pepper
- 5 :: Salt and pepper hash, no complexity
- 6 :: Salt and pepper hash, check against known weak passwords, enforce minimum complexity
- 7 :: Same but 2FA
* Expected RPG action
- Request audit/red team :: Reveal main flaw of current method/discuss price with other GM/roll against current technical level (make the price an advantage in the roll)
- Buy a leak/attack :: Run an auction/discuss price with other GMs, roll against current technical level/make the price an advantage
- Enforce password complexity :: Ask for details, do the worst possible job within what was specified, reassess the technical level
- Enforce 2FA :: Do it (in the worst possible specified way)
- Request social engineering :: Roll against social engineering defence
- Run social engineering training :: Increase social engineering defence
  - Training :: +1
  - Training + test :: +2
  - Training + continuous test/reward for reports :: +5
- Run phishing campaign :: Roll against social engineering defence
- Do a honeypot/tarpit :: Do it
* Run the simulation:
- Start dnsmasq
- Change the hostname of the VMs or simulation laptops
- Plug everything, make sure dnsmasq gives a lease to everyone, and that the dns works
- run =deploy.sh=
- on each machine, run =~/app/serve.sh=
- Hack one another.
- Implement request by linking files from the uppermost dir to each team's dir, or editing files there directly, before running =deploy.sh= again.
* Cyber range setup
See [[file:dnsmasq.conf]]
Set server's static ip:
: sudo ip addr add 192.168.1.1/24 dev enp0s25
You may need to tell NetworkManager to keep its dirty hands off your config:
: nmcli device set  enp0s25 managed no
This is useful if you notice a gnome resetting your configuration behind your back.
Then launch dnsmasq
: sudo dnsmasq --conf-file=dnsmasq.conf
* Snippets
** Serve with logs
: FLASK_APP=server.py FLASK_ENV=development flask run 2>&1 | tee -a Logs.txt
** ssh config
#+begin_src conf
host alice
	Hostname 192.168.1.147
host bob
	Hostname 192.168.1.162
host charlie
	Hostname 192.168.1.164
host dave
	Hostname 192.168.1.163
#+end_src
#+begin_src bash
for host in alice bob charlie dave
do
    ssh-copy-id user@$host
done
#+end_src
