#!/usr/bin/env python3
import hashlib

def poc(user, hostname, salt):
    """Return the proof of compromise"""
    answer = hashlib.sha256(user.encode()+salt.encode()+hostname.encode()).hexdigest()
    print(f"Generating secret for {user} {hostname} {salt} {answer}")
    return answer

def secret_for(user):
    """Return the user's secret in the current environment"""
    hostname = open("/etc/hostname").read().strip()
    salt = open("salt").read().strip()
    return poc(user, hostname, salt)
