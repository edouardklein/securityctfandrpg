#!/usr/bin/env python3

from flask import Flask, request
import uuid
import hashlib
from jinja2 import Environment, FileSystemLoader
jenv = Environment(loader=FileSystemLoader("."))

app = Flask(__name__)

auth_tokens = {}
mfa_tokens = {}

PASSWD = eval(open("passwd").read())

@app.route("/")
def slash():
    return jenv.get_template("plaintext.html").render()

@app.route("/mfa")
def mfa():
    user = request.cookies.get("user")
    return jenv.get_template("mfa.html").render(user=user)

@app.route("/mfa/auth/<user>", methods=["POST"])
def mfa_auth(user):
    "Return the mfa cookie value"
    user = request.json["user"]
    mfa = request.json["mfa"]
    try:
        assert user in mfa_tokens, f"Unknown user {user}"
        assert mfa_tokens[user][:4] == mfa, f"{user}'s mfa {mfa} does not match the one we sent."
        answer = mfa_tokens[user]
    except AssertionError as e:
        print(e)
        answer = "WRONGMFA"
    return {"mfa": answer}

@app.route("/auth/<user>", methods=["POST"])
def auth(user):
    "Return the auth cookie value"
    global auth_tokens
    global mfa_tokens
    user = request.json["user"]
    pwd = request.json["passwd"]
    try:
        assert user in PASSWD, f"Unknown user {user}"
        assert PASSWD[user] == pwd, f"{user}'s password {pwd} failed"
        answer = str(uuid.uuid4())
    except AssertionError:
        answer = "WRONGPASSWORD"
    auth_tokens[user] = answer
    mfa_tokens[user] = str(uuid.uuid4())
    print(f"MFA for {user} is {mfa_tokens[user]}")
    return {"token": answer}

@app.route("/mfa/secret")
def secret():
    "Return the secret, if the cookie is set"
    user = request.cookies.get("user")
    token = request.cookies.get("token")
    mfa = request.cookies.get("mfa")
    assert user in PASSWD, f"Unknown user {user}"
    assert user in auth_tokens, f"Unlogged user {user}, passwd is {auth_tokens}"
    assert auth_tokens[user] == token, f"Invalid token {token} for {user}, passwd is {auth_tokens}"
    assert user in mfa_tokens, f"Unmfaed user {user}, token is {mfa_tokens}"
    assert mfa == "WRONGMFA" or mfa_tokens[user] == mfa, f"Invalid mfa token {mfa} for user {user}"
    if token == "WRONGPASSWORD" or mfa == "WRONGMFA":
        false_secret = f"{user} on {open('/etc/hostname').read().strip()} " + hashlib.sha256(user.encode()+"toto".encode()).hexdigest()
        print(f"FALSE SECRET REVEALED: {false_secret}")
        return false_secret
    else:
        secret = f"{user} on {open('/etc/hostname').read().strip()} " + hashlib.sha256(user.encode()+open("salt","rb").read()+open('/etc/hostname', "rb").read()).hexdigest()
        print(f"SECRET REVEALED: {secret}")
        return secret


@app.route("/mfa/failed")
@app.route("/failed")
def failed():
    return "Login failed"
