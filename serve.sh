#!/usr/bin/env sh
export PATH=/opt/gnu/bin/:$PATH

FLASK_APP=clientside.py FLASK_ENV=development flask run --host=0.0.0.0 2>&1 | tee -a Logs.txt
