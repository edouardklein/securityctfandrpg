#!/gnu/store/d99ykvj3axzzidygsmdmzxah4lvxd6hw-bash-5.1.8/bin/bash
set -euxo pipefail

for host in alice bob charlie dave
do
    scp $host/* user@$host:app/
done
